package logica.SO;

import logica.Proceso;
import utilidades.Excepcion;

public class SeccionCritica {
	Proceso procesoEjecucion;
	// Son las rafagas realizadas para cada proceso, es decir entra se llega al
	// maximo
	// de rafagas y al terminar vuelve a
	int rafagasEjecutadas;
	SO so;

	/**
	 * Constructor se inicializa con 0 rafagas realizadas
	 * 
	 * @param so
	 */
	public SeccionCritica(SO so) {
		procesoEjecucion = null;
		rafagasEjecutadas = 0;
		this.so = so;
	}

	/**
	 * Agregar un proceso desde afuera, si ya esta llena botara una excepcion
	 * 
	 * @param proceso
	 */
	public void ingresarProceso(Proceso proceso) {
		try {
			this.agregar(proceso);
		} catch (Excepcion e) {
			e.printStackTrace();
		}

	}

	/**
	 * Se intenta agregar el proceso, si no se puede salta excepcion de que ya esta
	 * llena la seccion critica
	 * 
	 * @param proceso
	 * @throws Excepcion
	 */
	private void agregar(Proceso proceso) throws Excepcion {
		if (procesoEjecucion == null) {
			this.procesoEjecucion = proceso;
			this.procesoEjecucion.setT_comienzo(so.getTiempoActual());
			procesoEjecucion.setEstado("EN EJECUCION");
			rafagasEjecutadas = 0;
		} else {
			throw new Excepcion("Ya hay un proceso en seccion critica");
		}
	}

	/**
	 * Avanzar 1 rafaga en la ejecucion del proceso
	 * 
	 * @return si el proceso todavia tiene rafagas da null, si ya acabo devuelve el
	 *         proceso
	 */
	public Proceso avanzar() {
		// Si la rafaga del proceso sigue siendo mayor a la de rafagas ejecutadas por
		// la seccion critica
		if (procesoEjecucion.getT_rafaga() > rafagasEjecutadas) {
			// Aumento del tiempo del so
			so.aumentarTiempoActual(1);
			rafagasEjecutadas++;
			// Calculos temporales del proceso
			procesoEjecucion.setT_final(so.getTiempoActual());
			procesoEjecucion.setT_retorno(procesoEjecucion.getT_final() - procesoEjecucion.getT_llegada());
			procesoEjecucion.setT_espera(procesoEjecucion.getT_comienzo()-procesoEjecucion.getT_llegada());

			// Se retorna null para decir que el proceso aun no a acabado
			return null;
			// Si el proceso ya termino
		} else {
			Proceso proc = procesoEjecucion;
			limpiarSeccionCritica();
			return proc;
		}
	}

	public Proceso[] bloquear() {
		Proceso parteNoEjecutada = new Proceso(procesoEjecucion.getNombre(),
				procesoEjecucion.getT_rafaga() - rafagasEjecutadas, procesoEjecucion.getPrioridad());
		parteNoEjecutada.setT_llegada(procesoEjecucion.getT_llegada());

		Proceso ejecutado = procesoEjecucion;

		this.limpiarSeccionCritica();

		Proceso salida[] = new Proceso[2];
		salida[0] = ejecutado;
		salida[1] = parteNoEjecutada;
		System.out.println("TIEMPO LLEGADA NO EJECUTADO " + salida[1].getNombre() + ": " + salida[1].getT_llegada());
		return salida;
	}

	public void limpiarSeccionCritica() {
		procesoEjecucion = null;
		rafagasEjecutadas = 0;
	}

	public Proceso[] sacarProcesoActual() {
		Proceso parteNoEjecutada = new Proceso(procesoEjecucion.getNombre(),
				procesoEjecucion.getT_rafaga() - rafagasEjecutadas, procesoEjecucion.getPrioridad());
		parteNoEjecutada.setT_llegada(procesoEjecucion.getT_llegada());

		//Re calculo de los tiempos como se cambia la rafaga toca recalcular todo
		Proceso ejecutado = procesoEjecucion;
		ejecutado.setT_rafaga(rafagasEjecutadas);
		ejecutado.setT_final(so.getTiempoActual());
		ejecutado.setT_retorno(procesoEjecucion.getT_final() - procesoEjecucion.getT_llegada());
		ejecutado.setT_espera(
		ejecutado.getT_final() - procesoEjecucion.getT_llegada() - procesoEjecucion.getT_rafaga());

		this.limpiarSeccionCritica();

		Proceso salida[] = new Proceso[2];
		salida[0] = ejecutado;
		salida[1] = parteNoEjecutada;
		return salida;
	}
	public int getRafagasRestantes() {
		try {
			return procesoEjecucion.getT_rafaga()-rafagasEjecutadas;
		}catch(Exception e) {
			return 0;
		}
	}

	/**
	 * Validacion de si la seccion critica esta vacia o no
	 * 
	 * @return si esta bloqueada o no
	 */
	public boolean vacia() {
		if (procesoEjecucion == null) {
			return true;
		} else {
			return false;
		}
	}

	public String getNombre() {
			return procesoEjecucion.getNombre();
	
	}

	public String getT_llegada() {
		return String.valueOf(procesoEjecucion.getT_llegada());
	}

	public String getT_rafaga() {
		return String.valueOf(procesoEjecucion.getT_rafaga());
	}

	public String getT_comienzo() {
		return String.valueOf(procesoEjecucion.getT_comienzo());
	}

	public String getT_final() {
		return String.valueOf(procesoEjecucion.getT_final());
	}

	public String getT_retorno() {
		return String.valueOf(procesoEjecucion.getT_retorno());
	}

	public String getT_espera() {
		return String.valueOf(procesoEjecucion.getT_espera());
	}

}
