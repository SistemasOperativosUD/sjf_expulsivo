package logica.SO.gestionProcesos;

import logica.Proceso;
import logica.SO.SO;

public class GestionBloqueados {
	SO so;

	public GestionBloqueados(SO so) {
		this.so = so;
	}
	
	/**
	 * Sacar el proceso de la cola de bloqueados y ingresarlo a la cola de listos
	 */
	public void desbloquear() {
		Proceso desbloquear = so.getColaBloqueados().desencolarProceso();
		so.gestionProcesos().insertarProceso(desbloquear);
		
	}
	/**
	 * Decirle a la seccion critica que bloquee el proceso actual y devuelva la parte ejecutada y la que no
	 */
	public void bloquear() {
		Proceso partido[] =  so.getSeccionCritica().bloquear();
		so.getColaFinalizados().agregarProceso(partido[0]);
		so.getColaBloqueados().agregarProceso(partido[1]);
	}
}
