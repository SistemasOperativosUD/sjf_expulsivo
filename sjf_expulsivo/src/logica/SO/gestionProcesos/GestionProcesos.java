package logica.SO.gestionProcesos;

import logica.Cola;
import logica.Proceso;
import logica.SO.SO;
import utilidades.Excepcion;

public class GestionProcesos {

	SO so;

	public GestionProcesos(SO so) {
		this.so = so;
	}

	/**
	 * Insertar un proceso en el cual se establece su nuevo estado y se agrega su
	 * tiempo de llegada
	 *
	 * @param El proceso a ingresar
	 */
	public void insertarProceso(Proceso proceso) {

		// SFJ EXTENDIDO
		if (proceso.getT_rafaga() < so.getSeccionCritica().getRafagasRestantes()) {
			// Se saca el proceso actual y se mete la parte finalizada sin ejecutar a sus
			// respectivas colas
			Proceso cortados[] = so.getSeccionCritica().sacarProcesoActual();
			so.getColaFinalizados().agregarProceso(cortados[0]);
			so.getColaListos().agregarProceso(cortados[1]);

			// Se agrega el proceso que llego a la seccion critica
			if (proceso.getT_llegada() == -1) {
				proceso.setT_llegada(so.getTiempoActual());
			}
			so.getSeccionCritica().ingresarProceso(proceso);
		} else {
			// Si no entonces se mete a la cola de listos normales
			proceso.setEstado("LISTO");
			if (proceso.getT_llegada() == -1) {
				proceso.setT_llegada(so.getTiempoActual());
			}
			so.getColaListos().agregarProceso(proceso);
			// En caso que este vacia se hace mete a la cola de listos y se espera que se
			// avance
		}

	}

	/**
	 * Se obtiene el siguiente proceso de la lista deacuerdo a la prioridad del
	 * proceso
	 *
	 * @return La ubicacion del proceso en la lista
	 */
	public int getSiguienteProceso() {
		Cola listos = so.getColaListos();
		int idProceso = -1;
		int actualRafaga = 9999999;
		Proceso actual;
		for (int i = 0; i < listos.getTamano(); i++) {
			actual = listos.getElemento(i);
			if (actual.getT_rafaga() < actualRafaga) {
				actualRafaga = listos.getElemento(i).getT_rafaga();
				idProceso = i;
			}
		}
		return idProceso;
	}

	/**
	 * Front para ejecutar un proceso
	 * 
	 * @throws Excepcion
	 */
	public void agregarSiguienteSeccionCritica() {
		agregarSeccionCritica(getSiguienteProceso());
	}

	/**
	 * Se ejecuta un proceso, se saca de la cola de listos y se pasa a la cola de
	 * finalizados
	 *
	 * @param La ubicacion del proceso en la cola
	 * @throws Excepcion
	 */
	private void agregarSeccionCritica(int elementoCola) {
		try {
			Proceso proceso = so.getColaListos().desencolarProceso(elementoCola);
			so.getSeccionCritica().ingresarProceso(proceso);
		}catch(Exception e) {
			
		}
	}

	/**
	 * Se obtiene la cantidad total de procesos del SO
	 *
	 * @return Se retorna la cantidad total de finalizados y listos
	 */
	public int getCantidadProcesos() {
		Cola listos = so.getColaListos();
		Cola finalizados = so.getColaFinalizados();
		if (so.getSeccionCritica().vacia())
			return finalizados.getTamano() + listos.getTamano();
		else
			return finalizados.getTamano() + listos.getTamano() + 1;

	}

	/**
	 * Funcion que hace avanzar al programa en una unidad de tiempo
	 */
	public void siguienteSegundo() {
		// Si la seccion critica esta vacia se le mete otro proceso
		if (so.getSeccionCritica().vacia()) {
			this.agregarSiguienteSeccionCritica();
		}
		// Null si aun le quedan rafagas al proceso
		Proceso resultadoAvance = so.getSeccionCritica().avanzar();

		// Ya no hay que avanzar y la seccion escupe el proceso ejecutado, aqui se
		// guarda
		// Ese proceso en finalizados y se le mete uno nuevo c:
		if (resultadoAvance != null) {
			// Se agrega el proceso ya terminado a finalizados
			so.getColaFinalizados().agregarProceso(resultadoAvance);
			// Se agrega el siguiente proceso a seccion critica
			this.agregarSiguienteSeccionCritica();

		}
	}

}
