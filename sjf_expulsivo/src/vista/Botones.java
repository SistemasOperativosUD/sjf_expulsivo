package vista;

import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class Botones {
	JPanel panel = new JPanel();
	JButton btnAgregar = new JButton("agregar proceso");
	JButton btnSiguiente = new JButton("siguiente");
	JButton btnBloquear = new JButton("bloquear");
	JButton btnDesbloquear = new JButton("desbloquear");
	JTextField txtRafagas = new JTextField();

	public Botones(ActionListener listener) {
		btnAgregar.addActionListener(listener);
		btnSiguiente.addActionListener(listener);
		btnBloquear.addActionListener(listener);
		btnDesbloquear.addActionListener(listener);
		
		panel.add(btnAgregar);
		txtRafagas.setColumns(3);
		panel.add(txtRafagas);
		panel.add(btnSiguiente);
		panel.add(btnBloquear);
		panel.add(btnDesbloquear);
	}
	
	public JPanel getPanel() {
		return panel;
	}
	public JButton getBtnAgregar() {
		return btnAgregar;
	}
	public JButton getBtnSiguiente() {
		return btnSiguiente;
	}
	public JButton getBtnBloquear() {
		return btnBloquear;
	}
	public JButton getBtnDesbloquear() {
		return btnDesbloquear;
	}
	public JTextField getTxtRafagas() {
		return txtRafagas;
	}

}
