package vista;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import logica.Proceso;
import logica.SO.SO;
import utilidades.Aleatorio;

public class Grafica extends JFrame implements ActionListener {

    /* Logica */
    SO so = new SO();

    /* Elementos generales */
    Titulo titulo = new Titulo();
    Rejilla rejilla = new Rejilla(so);
    Tabla tabla = new Tabla();
    Botones botones = new Botones(this);
    TiempoActual tiempoActual = new TiempoActual(so);
    Colas colas = new Colas(so);
    
    /*Utilidades*/
    Aleatorio aleatorio = new Aleatorio();

    public Grafica() {
        this.setLayout(new GridBagLayout());
        this.setSize(1200, 500);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLocationRelativeTo(this);
        this.cargarElementos();
        this.setVisible(true);
    }

    public void cargarElementos() {
        GridBagConstraints c = new GridBagConstraints();
        c.weightx = 1.0;
        c.weighty = 1.0;
        c.fill = GridBagConstraints.BOTH;
        c.insets = new Insets(5, 5, 5, 5);

        c.gridx = 0; // El área de texto empieza en la columna cero.
        c.gridy = 0; // El área de texto empieza en la fila cero
        c.gridwidth = 7; // El área de texto ocupa dos columnas.
        c.gridheight = 1; // El área de texto ocupa 2 filas.
        this.add(titulo.getPanel(), c);
        c.gridx = 0;
        c.gridy = 1;
        c.gridheight = 3;
        c.gridwidth = 7;
        this.add(rejilla.getPanel(), c);
        c.gridx = 0;
        c.gridy = 4;
        c.gridheight = 1;
        c.gridwidth = 7;
        this.add(colas.getPanel(), c);
        c.gridx = 0;
		c.gridy = 5;
		c.gridheight = 1;
		c.gridwidth = 7;
		this.add(tiempoActual.getPanel(),c);
        c.gridx = 0;
        c.gridy = 6;
        c.gridheight = 5;
        c.gridwidth = 7;
        this.add(tabla.getPanel(), c);
        c.fill = GridBagConstraints.BOTH;
        c.gridx = 0;
        c.gridy = 11;
        c.gridheight = 1;
        c.gridwidth = 7;
        this.add(botones.getPanel(), c);

    }

    public void actualizarGrafica() {
        rejilla.actualizarDimensiones();
        tabla.repintarTabla(so.datos().getDatosTabla());
        colas.actualizarColas();
        tiempoActual.actualizarTiempoActual();
        this.getContentPane().validate();
        this.getContentPane().repaint();

    }

    public void nuevoProceso(String nombre, int rafaga) {
        so.gestionProcesos().insertarProceso(new Proceso(nombre, rafaga));
        //so.insertarProceso(new Proceso(nombre, getRandomRafaga()));
        actualizarGrafica();
    }
    public void nuevoProceso() {
        so.gestionProcesos().insertarProceso(new Proceso(getRandomNombre(), getRandomRafaga()));
        actualizarGrafica();

    }
    public void nuevoProceso(int rafaga) {
        so.gestionProcesos().insertarProceso(new Proceso(getRandomNombre(), rafaga));
        actualizarGrafica();

    }

    public void siguienteIteracion() {
        so.gestionProcesos().siguienteSegundo();
        actualizarGrafica();
    }

    public void bloquear() {
        so.gestionBloqueados().bloquear();
    }

    private String getRandomNombre() {
        return aleatorio.getRandomString(2);
    }

    private int getRandomRafaga() {
        return aleatorio.getRandomNumero(3, 7);
    }

    private int getRandomPrioridad() {
        return aleatorio.getRandomNumero(1, 3);

    }

    private void desbloquear() {
        so.gestionBloqueados().desbloquear();
        actualizarGrafica();
    }
    
    /**
     * Listeners para cada boton
     */
    @Override
    public void actionPerformed(ActionEvent e) {

        if (e.getSource() == botones.getBtnAgregar()) {
        	//Si no hay nada en el texto la rafaga se general al azar
        	if(botones.getTxtRafagas().getText().equals("")) {
        		this.nuevoProceso();
        	}else {
        		this.nuevoProceso(Integer.parseInt(botones.getTxtRafagas().getText()));
        	}
        	//Se limpia el txt pase lo que pase
        	botones.getTxtRafagas().setText("");
        } else if (e.getSource() == botones.getBtnSiguiente()) {
            siguienteIteracion();
        } else if (e.getSource() == botones.getBtnBloquear()) {
            this.bloquear();
            JOptionPane.showMessageDialog(null, "El proceso sufrio una interrupcion inesperada");
        } else if (e.getSource() == botones.getBtnDesbloquear()) {
            this.desbloquear();
        }

    }

}
